fx = 5.7197374996822771e+02;
cx = 3.1950000000000000e+02;
fy = 5.7100733178339351e+02;
cy = 2.3950000000000000e+02;


num_of_boards = 2;


path = '/host/aly/Kinect_Matlab/Dataset/3XboxControllers/';

mkdir([path 'Annotation_New']);

data_fn = [path 'test.txt'];

fid = fopen(data_fn, 'r');

tline = fgetl(fid);

while ischar(tline)
    

    fn = sprintf('%s.txt', tline);
    [path 'Annotation_New/' fn]
    a_fid = fopen([path 'Annotation_New/' fn], 'w');
    fprintf(a_fid, '%d\n', num_of_boards);
    
    for i=1:num_of_boards
        fn = sprintf('%s_%d.txt', tline, i-1);
        M = csvread([path 'Marker/' fn]);
        M1 = M(:,1:3);
        M2 = M(:,4:6);
        [T, Eps] = estimateRigidTransform(M1', M2');
        
        T(1,4) = (T(1,4) - cx)*T(3,4)/fx;
        T(2,4) = (T(2,4) - cy)*T(3,4)/fy;

        for xx=1:4
            for yy=1:4
                if yy==4
                    fprintf(a_fid, '%f\n', T(xx,yy));
                else
                    fprintf(a_fid, '%f,', T(xx,yy));
                end
            end
        end
    end
    
    fclose(a_fid);
    
    tline = fgetl(fid);     
end

fclose(fid);
