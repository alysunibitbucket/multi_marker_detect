/*****************************
Copyright 2011 Rafael Muñoz Salinas. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY Rafael Muñoz Salinas ''AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Rafael Muñoz Salinas OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Rafael Muñoz Salinas.
********************************/
#include <iostream>
#include <fstream>
#include <sstream>
//#include <opencv/cv.h>
//#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include "aruco.h"
#include "boarddetector.h"
#include "cvdrawingutils.h"
#include <string>
#include <fstream>

using namespace cv;
using namespace aruco;


//const float MarkerSize = 0.034;
//const float MarkerSize = 0.044;
const float MarkerSize = 0.0575;
//const string camParamsFileName = "xtion_pro.yml";
const string camParamsFileName = "primesense.yml";
const char* boardParamsFileName = "Markers.Ver.2/boardConfiguration";
const char* inputImageDirectory = "/host/aly/Kinect_Matlab/Dataset/3XboxControllers/RGB/";
const char* inputDepthDirectory = "/host/aly/Kinect_Matlab/Dataset/3XboxControllers/Depth/";
const char* outputImageDirectory = "/host/aly/Kinect_Matlab/Dataset/3XboxControllers/Label/";
const char* outputMarkerDirectory = "/host/aly/Kinect_Matlab/Dataset/3XboxControllers/Marker/";
const char* outputDataDirectory = "/host/aly/Kinect_Matlab/Dataset/3XboxControllers/Annotation_New/";
const char* inputDataListFilename = "/host/aly/Kinect_Matlab/Dataset/3XboxControllers/test.txt";
//const string outputDataDirectory = "/home/rigas/workspace/MarkerDetectGL/bin/Debug/data";

//const int num_of_boards = 2; //  Joystick, Shampoo, XboxController, Stapler
const int num_of_boards = 3; // 3XboxControllers, Camera, Cup, Mouse, Mug, Pipe, SweetsBox, JuiceCarton, MilkCarton

void saveTransfMatrix(ofstream &file, Mat& R, Mat &t)
{
    // homogenous transformation matrix
    // hMat = [R t; 0 1]
    Mat hMat = Mat::zeros(4, 4, CV_32F);

    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            hMat.at<float>(i,j) = R.at<float>(i,j);
            if(j == 2)
                hMat.at<float>(i,j+1) = t.at<float>(i);
        }
    }

    hMat.at<float>(3,3) = 1;

    file << format(hMat,"csv");
}


int main(int argc,char **argv)
{


	try
	{
//		//if(argc<3) {cerr<<"Usage: image  boardConfig.yml [cameraParams.yml] [markerSize]  [outImage]"<<endl;exit(0);}
//
//		VideoCapture capture(0);
//
//		if(!capture.isOpened())
//		{
//			cout << "cannot open capture" << endl;
//			return -1;
//		}


        char buffer[400];

        // Create data directories
        sprintf(buffer, "mkdir %s", outputMarkerDirectory);
        system(buffer);
        sprintf(buffer, "mkdir %s", outputDataDirectory);
        system(buffer);

        ifstream inputDataListFile;
        inputDataListFile.open(inputDataListFilename);

		CameraParameters camParams;


		vector<BoardConfiguration> TheBoardConfigs(3);
		BoardDetector TheBoardDetector;


        char BoardConfigFilename[200];
        for(int i=0;i<3;++i)
        {
            sprintf(BoardConfigFilename, "%s_%d.yml", boardParamsFileName, i);
            TheBoardConfigs[i].readFromFile(BoardConfigFilename);
        }

		// read in camera parameters
		camParams.readFromXMLFile(camParamsFileName);

        int n = 0;
		while (!inputDataListFile.eof())
		{
            cout << "Processing image #" << n << endl;
            string fn;
            getline(inputDataListFile,fn);

            sprintf(buffer, "%s/%s.png", inputImageDirectory, fn.c_str());

            Mat bgrImage = imread(buffer);

            sprintf(buffer, "%s/%s.png", inputDepthDirectory, fn.c_str());

            Mat depthImage = imread(buffer, CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);

			if(bgrImage.empty() || depthImage.empty())
				break;

            vector<Marker> Markers;

            // resizes the parameters to fit the size of the input image
            //camParams.resize( bgrImage.size());

            MarkerDetector MDetector;
            MDetector.setMinMaxSize(0.001, .9);
            //MDetector.setThresholdMethod(MDetector.CANNY);
			MDetector.detect(bgrImage, Markers);

            vector<vector<Point3f > > Markers3D;

            for(size_t k = 0; k<num_of_boards; ++k)
            {

                sprintf(buffer, "%s/%s_%d.txt", outputMarkerDirectory, fn.c_str(), k);
                ofstream outputMarkerFile;

                outputMarkerFile.open(buffer);

                // draw boundaries for each marker
                for(size_t i = 0; i < Markers.size(); i++)
                {
                    // Retrieve z value for marker corners
                    try{
                        vector<Point3f > currentMarker3D;

                        MarkerInfo m = TheBoardConfigs[k].getMarkerInfo(Markers[i].id);

                        for(size_t j = 0; j<Markers[i].size(); ++j)
                        {
                            Point3f p(Markers[i][j].x, Markers[i][j].y, 0);
                            p.z = (float)depthImage.at<short>(p.y,p.x);
                            currentMarker3D.push_back(p);

                            // For debug
//                            int index = TheBoardConfigs[k].getIndexOfMarkerId(Markers[i].id);
//
//                            if(index!=-1)
//                            {
//                                Scalar color;
//                                switch(k)
//                                {
//                                case 0:
//                                    color = Scalar(255,0,0);
//                                    break;
//                                case 1:
//                                    color = Scalar(0,255,0);
//                                    break;
//                                case 2:
//                                    color = Scalar(0,0,255);
//                                    break;
//                                }
//                                Markers[i].draw(bgrImage, color, 2,true);
//                            }
//                            // Ends debug

                            outputMarkerFile << p.x << ',' << p.y << ',' << p.z << ',' << m[j].x << ',' << m[j].y << ',' << m[j].z << endl;
                        }
                    }catch(...)
                    {

                    }

                }

                outputMarkerFile.close();
            }
            //imshow("",bgrImage);


//			imshow("bgrOutput", bgrImage);
//            waitKey(1);

			//if(waitKey(30) >= 0) break;

//            if(n==19)
//            {
//                imshow("", bgrImage);
//                waitKey(0);
//            }

            bgrImage.release();
            depthImage.release();

            n++;

		}
	} catch(exception &e) {
		cout << "Exception thrown: " << e.what() << endl;
	}

    return 0;
}
