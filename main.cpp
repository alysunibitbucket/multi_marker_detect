/*****************************
Copyright 2011 Rafael Muñoz Salinas. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY Rafael Muñoz Salinas ''AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Rafael Muñoz Salinas OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Rafael Muñoz Salinas.
********************************/
#include <iostream>
#include <fstream>
#include <sstream>
//#include <opencv/cv.h>
//#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include "aruco.h"
#include "boarddetector.h"
#include "cvdrawingutils.h"
#include <string>
#include <fstream>
#include <OpenNI.h>

using namespace cv;
using namespace aruco;


//const float MarkerSize = 0.0575;
const float MarkerSize = 0.044;
//const float MarkerSize = 0;

const string camParamsFileName = "primesense.yml";

//const string camParamsFileName = "xtion_pro.yml";
const char* boardParamsFileName = "Markers.Ver.2/boardConfiguration";
const char* outputRGBDirectory = "/host/aly/Kinect_Matlab/Dataset/Binpicking/Juice/RGB/";
const char* outputDepthDirectory = "/host/aly/Kinect_Matlab/Dataset/Binpicking/Juice/Depth/";
const char* outputLabelDirectory = "/host/aly/Kinect_Matlab/Dataset/Binpicking/Juice/Label/";
const char* outputDataDirectory = "/host/aly/Kinect_Matlab/Dataset/Binpicking/Juice/Annotation/";
const char* outputDataListFilename = "/host/aly/Kinect_Matlab/Dataset/Binpicking/Juice/test.txt";

//const char* boardParamsFileName = "Markers.Ver.2/boardConfiguration";
//const char* outputRGBDirectory = "/host/aly/Kinect_Matlab/Dataset.Test.2/RGB/";
//const char* outputDepthDirectory = "/host/aly/Kinect_Matlab/Dataset.Test.2/Depth/";
//const char* outputLabelDirectory = "/host/aly/Kinect_Matlab/Dataset.Test.2/Label/";
//const char* outputDataDirectory = "/host/aly/Kinect_Matlab/Dataset.Test.2/Annotation/";
//const char* outputDataListFilename = "/host/aly/Kinect_Matlab/Dataset.Test.2/test.txt";



const int num_of_boards = 0;

void saveTransfMatrix(ofstream &file, Mat& R, Mat &t)
{
    // homogenous transformation matrix
    // hMat = [R t; 0 1]
    Mat hMat = Mat::zeros(4, 4, CV_32F);

    for(int i = 0; i < 3; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            hMat.at<float>(i,j) = R.at<float>(i,j);
            if(j == 2)
                hMat.at<float>(i,j+1) = t.at<float>(i)*1000;
        }
    }

    hMat.at<float>(3,3) = 1;

    file << format(hMat,"csv");
}


void convert2Display(Mat& depthImage, Mat& displayImage)
{
    const short MAX_DEPTH = SHRT_MAX;
    float pDepthHist[MAX_DEPTH] = {0};
    // Calculate the accumulative histogram (the yellow display...)

    unsigned int nNumberOfPoints = 0;
    for (unsigned int y = 0; y < depthImage.rows; ++y)
    {
        short* row_ptr = depthImage.ptr<short>(y);

        for (unsigned int x = 0; x < depthImage.cols; ++x)
        {
            if (row_ptr[x] != 0)
            {
                pDepthHist[row_ptr[x]]++;
                nNumberOfPoints++;
            }
        }
    }

    for (int nIndex=1; nIndex<MAX_DEPTH; nIndex++)
    {
        pDepthHist[nIndex] += pDepthHist[nIndex-1];
    }
    if (nNumberOfPoints)
    {
        for (int nIndex=1; nIndex<MAX_DEPTH; nIndex++)
        {
            pDepthHist[nIndex] = (unsigned int)(256 *(1.0f - (pDepthHist[nIndex] / nNumberOfPoints)));
        }
    }


    for (unsigned int y = 0; y < depthImage.rows; ++y)
    {
        short* row_ptr = depthImage.ptr<short>(y);

        for (unsigned int x = 0; x < depthImage.cols; ++x)
        {
            if (row_ptr[x] != 0)
            {
                Vec3b rgbVals = displayImage.at<Vec3b>(y, x);

                int nHistValue = pDepthHist[row_ptr[x]];
                rgbVals[0] = nHistValue;
                rgbVals[1] = nHistValue;
                rgbVals[2] = 0;
                displayImage.at<Vec3b>(y, x) = rgbVals;
            }
        }
    }


}

int main(int argc,char **argv)
{

/*
    cv::Mat r = cv::imread("/host/aly/Kinect_Matlab/Dataset/tmp/Label/img_000.png", CV_LOAD_IMAGE_ANYCOLOR);
    cv::Mat d = cv::imread("/host/aly/Kinect_Matlab/Dataset/tmp/Depth/img_000.png", CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
    cv::circle(r, cv::Point(267, 253), 1, cv::Scalar(0,0,255), 2);
    std::cout <<" depth at point (y,x) (267, 253) is" <<  d.at<int16_t>(253, 267) << std::endl;
    cv::imshow("r", r);
    cv::waitKey(0);
    exit(-1);

*/

    try
    {
        char buffer[400];

        // Create data directories
        sprintf(buffer, "mkdir %s", outputRGBDirectory);
        system(buffer);
        sprintf(buffer, "mkdir %s", outputDepthDirectory);
        system(buffer);
        sprintf(buffer, "mkdir %s", outputLabelDirectory);
        system(buffer);
        sprintf(buffer, "mkdir %s", outputDataDirectory);
        system(buffer);

        // Create display windows and move them to proper positions
        namedWindow("DepthOutput");
        moveWindow("DepthOutput", 700, 500);
        namedWindow("LabelOutput");
        moveWindow("LabelOutput", 0, 500);

        /****************************************** Initialise OpenNI devices ****************************/
        openni::Status rc = openni::STATUS_OK;
        openni::VideoFrameRef		m_depthFrame;
        openni::VideoFrameRef		m_colorFrame;

        openni::Device			    m_device;
        openni::VideoStream		m_depthStream;
        openni::VideoStream		m_colorStream;
        openni::VideoStream**		m_streams;
        const char* deviceURI = openni::ANY_DEVICE;

        int			m_width;
        int			m_height;

        rc = openni::OpenNI::initialize();

        printf("After initialization:\n%s\n", openni::OpenNI::getExtendedError());

        rc = m_device.open(deviceURI);
        if (rc != openni::STATUS_OK)
        {
            printf("SimpleViewer: Device open failed:\n%s\n", openni::OpenNI::getExtendedError());
            openni::OpenNI::shutdown();
            return 1;
        }

        rc = m_depthStream.create(m_device, openni::SENSOR_DEPTH);
        if (rc == openni::STATUS_OK)
        {
            rc = m_depthStream.start();
            if (rc != openni::STATUS_OK)
            {
                printf("SimpleViewer: Couldn't start depth stream:\n%s\n", openni::OpenNI::getExtendedError());
                m_depthStream.destroy();
            }
        }
        else
        {
            printf("SimpleViewer: Couldn't find depth stream:\n%s\n", openni::OpenNI::getExtendedError());
        }

        rc = m_colorStream.create(m_device, openni::SENSOR_COLOR);
        if (rc == openni::STATUS_OK)
        {
            rc = m_colorStream.start();
            if (rc != openni::STATUS_OK)
            {
                printf("SimpleViewer: Couldn't start color stream:\n%s\n", openni::OpenNI::getExtendedError());
                m_colorStream.destroy();
            }
        }
        else
        {
            printf("SimpleViewer: Couldn't find color stream:\n%s\n", openni::OpenNI::getExtendedError());
        }

        if (!m_depthStream.isValid() || !m_colorStream.isValid())
        {
            printf("SimpleViewer: No valid streams. Exiting\n");
            openni::OpenNI::shutdown();
            return 2;
        }

        openni::VideoMode depthVideoMode;
        openni::VideoMode colorVideoMode;

        if (m_depthStream.isValid() && m_colorStream.isValid())
        {
            depthVideoMode = m_depthStream.getVideoMode();
            colorVideoMode = m_colorStream.getVideoMode();

            int depthWidth = depthVideoMode.getResolutionX();
            int depthHeight = depthVideoMode.getResolutionY();
            int colorWidth = colorVideoMode.getResolutionX();
            int colorHeight = colorVideoMode.getResolutionY();

            if (depthWidth == colorWidth &&
                    depthHeight == colorHeight)
            {
                m_width = depthWidth;
                m_height = depthHeight;
            }
            else
            {
                printf("Error - expect color and depth to be in same resolution: D: %dx%d, C: %dx%d\n",
                       depthWidth, depthHeight,
                       colorWidth, colorHeight);
                return openni::STATUS_ERROR;
            }
        }
        else if (m_depthStream.isValid())
        {
            depthVideoMode = m_depthStream.getVideoMode();
            m_width = depthVideoMode.getResolutionX();
            m_height = depthVideoMode.getResolutionY();
        }
        else if (m_colorStream.isValid())
        {
            colorVideoMode = m_colorStream.getVideoMode();
            m_width = colorVideoMode.getResolutionX();
            m_height = colorVideoMode.getResolutionY();
        }
        else
        {
            printf("Error - expects at least one of the streams to be valid...\n");
            return openni::STATUS_ERROR;
        }

        m_streams = new openni::VideoStream*[2];
        m_streams[0] = &m_depthStream;
        m_streams[1] = &m_colorStream;


        /********************************************* Main process *******************************/

        ofstream outputDataListFile;
        outputDataListFile.open(outputDataListFilename);


#ifdef OPENNI1
        VideoCapture capture(CV_CAP_OPENNI);
        capture.set( CV_CAP_OPENNI_IMAGE_GENERATOR_OUTPUT_MODE, CV_CAP_OPENNI_VGA_30HZ );
        capture.set(CV_CAP_PROP_OPENNI_REGISTRATION, 1);

        if(!capture.isOpened())
        {
            cout << "cannot open capture" << endl;
            return -1;
        }

        cout << "\nDepth generator output mode:" << endl <<
             "FRAME_WIDTH      " << capture.get( CV_CAP_PROP_FRAME_WIDTH ) << endl <<
             "FRAME_HEIGHT     " << capture.get( CV_CAP_PROP_FRAME_HEIGHT ) << endl <<
             "FRAME_MAX_DEPTH  " << capture.get( CV_CAP_PROP_OPENNI_FRAME_MAX_DEPTH ) << " mm" << endl <<
             "FPS              " << capture.get( CV_CAP_PROP_FPS ) << endl <<
             "REGISTRATION     " << capture.get( CV_CAP_PROP_OPENNI_REGISTRATION ) << endl;
        if( capture.get( CV_CAP_OPENNI_IMAGE_GENERATOR_PRESENT ) )
        {
            cout <<
                 "\nImage generator output mode:" << endl <<
                 "FRAME_WIDTH   " << capture.get( CV_CAP_OPENNI_IMAGE_GENERATOR+CV_CAP_PROP_FRAME_WIDTH ) << endl <<
                 "FRAME_HEIGHT  " << capture.get( CV_CAP_OPENNI_IMAGE_GENERATOR+CV_CAP_PROP_FRAME_HEIGHT ) << endl <<
                 "FPS           " << capture.get( CV_CAP_OPENNI_IMAGE_GENERATOR+CV_CAP_PROP_FPS ) << endl;
        }
        else
        {
            cout << "\nDevice doesn't contain image generator." << endl;
            return 0;
        }


#endif

        CameraParameters camParams;
        MarkerDetector MDetector;
        MDetector.setMinMaxSize(0.001, .9);

        vector<Marker> Markers;

        vector<BoardConfiguration> TheBoardConfigs(num_of_boards);
        BoardDetector TheBoardDetector;


        char BoardConfigFilename[200];
        for(int i=0; i<num_of_boards; ++i)
        {
            sprintf(BoardConfigFilename, "%s_%d.yml", boardParamsFileName, i);
            TheBoardConfigs[i].readFromFile(BoardConfigFilename);
        }

        // read in camera parameters
        camParams.readFromXMLFile(camParamsFileName);

        Mat labelImage = Mat::zeros(depthVideoMode.getResolutionY(), depthVideoMode.getResolutionX(), CV_8UC3);
        Mat bgrImage = Mat::zeros(depthVideoMode.getResolutionY(), depthVideoMode.getResolutionX(), CV_8UC3);
        Mat depthImage = Mat::zeros(depthVideoMode.getResolutionY(), depthVideoMode.getResolutionX(), CV_16UC1);

        //camParams.resize( Size(480, 640));

        size_t frameNo = 0;

        bool recording = false;

        for(;;)
        {

#ifdef OPENNI1
            capture.grab();
            //capture.retrieve( depthMap, CV_CAP_OPENNI_DEPTH_MAP );
            capture.retrieve( bgrImage, CV_CAP_OPENNI_BGR_IMAGE );
            capture.retrieve( depthImage, CV_CAP_OPENNI_DEPTH_MAP );

#else
            // Read a new frame
            m_streams[0]->readFrame(&m_depthFrame);
            m_streams[1]->readFrame(&m_colorFrame);

            const openni::DepthPixel* pDepth = (const openni::DepthPixel*)m_depthFrame.getData();
            const openni::RGB888Pixel* pImage = (const openni::RGB888Pixel*)m_colorFrame.getData();

            //CreateFilteredDepthArray(g_depthFrame, imgDepth16u);

            Mat rgbImage = Mat::zeros(depthVideoMode.getResolutionY(), depthVideoMode.getResolutionX(), CV_8UC3);
            memcpy(rgbImage.data, pImage, 3*depthVideoMode.getResolutionY()*depthVideoMode.getResolutionX()*sizeof(uint8_t));
            memcpy(depthImage.data, pDepth, depthVideoMode.getResolutionY()*depthVideoMode.getResolutionX()*sizeof(short));

            cvtColor(rgbImage, bgrImage, CV_RGB2BGR);
            rgbImage.release();

#endif


//            char buffer[400];
//            sprintf(buffer, "%s/img_%03d.png", inputDataDirectory, 25);
//
          //  Mat bgrImage = imread(buffer, CV_CAP_OPENNI_BGR_IMAGE);

            if(bgrImage.empty() || depthImage.empty())
                break;
            bgrImage.copyTo(labelImage);

            MDetector.detect(bgrImage, Markers);

            // draw boundaries for each marker
            for(size_t j = 0; j < Markers.size(); j++)
            {


                for(int i=0; i<TheBoardConfigs.size(); ++i)
                {
                    int index = TheBoardConfigs[i].getIndexOfMarkerId(Markers[j].id);
                    if(index!=-1)
                    {
                        Scalar color;
                        switch(i)
                        {
                        case 0:
                            color = Scalar(255,0,0);
                            break;
                        case 1:
                            color = Scalar(0,255,0);
                            break;
                        case 2:
                            color = Scalar(0,0,255);
                            break;
                        }
                        Markers[j].draw(labelImage, color, 2,true);
                    }
                }
            }


            int key = 0xff & waitKey(50);

            if(recording == false)
            {
                putText(labelImage, "Press s to start recording, e to end recording, q to quit", Point(10,bgrImage.rows-10), CV_FONT_HERSHEY_SIMPLEX, .6, Scalar(0, 255, 0));

                if( key == 's' )
                    recording = true;

                if( (key == 27 || key == 'q'))
                    break;

            }
            else
            {
                if(key == 'e'|| key == 'q' || key == 27)
                {
                    recording = false;
                    break;
                }
            }

            int numOfDetectedBoards = 0;

            sprintf(buffer, "%s/img_%03d.txt", outputDataDirectory, frameNo);

            ofstream outputAnnotationFile;

            vector<Board> TheBoardDetecteds(num_of_boards);

            for(int i=0; i<num_of_boards; ++i)
            {


                Board b;
                float probDetect=TheBoardDetector.detect( Markers, TheBoardConfigs[i],b, camParams,MarkerSize);
                TheBoardDetecteds[i] = b;

                if(probDetect>0)
                {

                    // draw the world axes in the centre of the board
                    CvDrawingUtils::draw3dAxis(labelImage,TheBoardDetecteds[i],camParams);

                    if(recording)
                    {

                        //draw a 3d cube in each marker if there is 3d info
                        if (camParams.isValid())
                        {
//                            for(unsigned int i=0; i<Markers.size(); i++)
//                            {
//                                //CvDrawingUtils::draw3dCube(bgrImage,Markers[i],camParams);
//                                //CvDrawingUtils::draw3dAxis(bgrImage,Markers[i],camParams);
//                            }



                        }
                    }

                    numOfDetectedBoards++;
                }
            }

            if(recording && numOfDetectedBoards==num_of_boards)
            {

                outputAnnotationFile.open(buffer);

                outputAnnotationFile << num_of_boards << endl;

                for(int i=0; i<num_of_boards; ++i)
                {
                    // our rotation matrix
                    Mat Rot3D;
                    // convert 3x1 rotation vector to 3x3 rotation matrix

                    Board b = TheBoardDetecteds[i];

                    Rodrigues(b.Rvec, Rot3D);

                    saveTransfMatrix(outputAnnotationFile, Rot3D, b.Tvec);
                    float world_x = b.Tvec.at<float>(0) * 1000;
                    float world_y = b.Tvec.at<float>(1) * 1000;
                    //float world_z = b.Tvec.at<float>(2) * 1000 * (1-.081);
                    float world_z = b.Tvec.at<float>(2) * 1000;

                    float fx = 571.9737;
                    float fy = 571.0073;
                    float cx =  319.5000;
                    float cy = 239.5000;

                    int pix_x = (world_x/world_z)*fx + cx;
                    int pix_y = (world_y/world_z)*fy + cy;
                    //std::cout << "depth image: " << depthImage.at<int16_t>(pix_y, pix_x) << " world_z: " << world_z << " diff is " << world_z - depthImage.at<int16_t>(pix_y, pix_x) ;
                    //std::cout << " " << (world_z - depthImage.at<int16_t>(pix_y, pix_x))/float(depthImage.at<int16_t>(pix_y, pix_x))<< std::endl;
                    //std::cout << depthImage.at<int16_t>(240, 320) << std::endl;

                    Rot3D.release();
                }


                outputAnnotationFile.close();

                sprintf(buffer, "%s/img_%03d.png", outputRGBDirectory, frameNo);
                imwrite(buffer, bgrImage);

                sprintf(buffer, "%s/img_%03d.png", outputDepthDirectory, frameNo);
                imwrite(buffer, depthImage);

                sprintf(buffer, "%s/img_%03d.png", outputLabelDirectory, frameNo);
                imwrite(buffer, labelImage);

                sprintf(buffer, "img_%03d", frameNo);
                outputDataListFile << buffer << endl;

                frameNo++;

            }

            stringstream ss;
            ss << "F: " << frameNo;
            putText(labelImage, ss.str(), Point(10,50), CV_FONT_HERSHEY_COMPLEX, 1, Scalar(0, 255, 255));
            imshow("LabelOutput", labelImage);
            //ss.clear();

            const float scaleFactor = 0.05f;
            Mat displayImage = Mat::zeros(depthImage.rows, depthImage.cols, CV_8UC3);
            //depthImage.convertTo( displayImage, CV_8UC1, scaleFactor );
            convert2Display(depthImage, displayImage);
            imshow("DepthOutput", displayImage);
            waitKey(1);



        }

    }
    catch(exception &e)
    {
        cout << "Exception thrown: " << e.what() << endl;
    }

    return 0;
}
