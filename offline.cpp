
/*****************************
Copyright 2011 Rafael Muñoz Salinas. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY Rafael Muñoz Salinas ''AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Rafael Muñoz Salinas OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Rafael Muñoz Salinas.
********************************/
#include <iostream>
#include <fstream>
#include <sstream>
//#include <opencv/cv.h>
//#include <opencv/highgui.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include "aruco.h"
#include "boarddetector.h"
#include "cvdrawingutils.h"
#include <string>
#include <fstream>

using namespace cv;
using namespace aruco;


//const float MarkerSize = 0.034;
const float MarkerSize = 0.044;
const string camParamsFileName = "primesense.yml";
const char* boardParamsFileName = "Markers.Ver.2/boardConfiguration";
const char* outputRGBDirectory = "/host/aly/Kinect_Matlab/Dataset/tmp/RGB/";
const char* outputDepthDirectory = "/host/aly/Kinect_Matlab/Dataset/tmp/Depth/";
const char* outputLabelDirectory = "/host/aly/Kinect_Matlab/Dataset/tmp/Label/";
const char* outputDataDirectory = "/host/aly/Kinect_Matlab/Dataset/tmp/Annotation/";
const char* outputDataListFilename = "/host/aly/Kinect_Matlab/Dataset.Test.2/test.txt";
//const string outputDataDirectory = "/home/rigas/workspace/MarkerDetectGL/bin/Debug/data";

void saveTransfMatrix(ofstream &file, Mat& R, Mat &t)
{
    // homogenous transformation matrix
    // hMat = [R t; 0 1]
    Mat hMat = Mat::zeros(4, 4, CV_32F);

    for(int i = 0; i < 3; i++) {
        for(int j = 0; j < 3; j++) {
            hMat.at<float>(i,j) = R.at<float>(i,j);
            if(j == 2)
                hMat.at<float>(i,j+1) = t.at<float>(i,0);
        }
    }

    hMat.at<float>(3,3) = 1;

    file << hMat;
}


int main(int argc,char **argv)
{
    unsigned numBoards = 3;
    bool boardDetect[numBoards];
    Mat RotationMatrics[3];
    Mat TransfMatrices[3];


    for(int i = 0; i < numBoards; i++)
        boardDetect[i] = false;

	try
	{
        ofstream outputDataListFile;
        outputDataListFile.open(outputDataListFilename);

		CameraParameters camParams;


		vector<BoardConfiguration> TheBoardConfigs(3);
		BoardDetector TheBoardDetector;


        char BoardConfigFilename[200];
        for(int i=0;i<3;++i)
        {
            sprintf(BoardConfigFilename, "%s_%d.yml", boardParamsFileName, i);
            TheBoardConfigs[i].readFromFile(BoardConfigFilename);
        }

		// read in camera parameters
		camParams.readFromXMLFile(camParamsFileName);

        char buffer[400];

		for(int n=0;n<=100;++n)
		{
            cout << "Processing image #" << n << endl;
            sprintf(buffer, "%s/img_%03d.png", inputDataDirectory, n+1);

            Mat bgrImage = imread(buffer);

			if(bgrImage.empty())
				break;

            vector<Marker> Markers;

            // resizes the parameters to fit the size of the input image
            //camParams.resize( bgrImage.size());

            MarkerDetector MDetector;
            MDetector.setMinMaxSize(0.001, .9);
            //MDetector.setThresholdMethod(MDetector.CANNY);
			MDetector.detect(bgrImage, Markers);


            // draw boundaries for each marker
            for(size_t i = 0; i < Markers.size(); i++)
            {
                //cout << Markers[i] << endl;
                Markers[i].draw(bgrImage, Scalar(0,255,0), 2);
            }
            for(int i=0;i<3;++i)
            {

                Board TheBoardDetected;

                Mat Rot3D;
                Mat Trans;

                float probDetect=TheBoardDetector.detect( Markers, TheBoardConfigs[i], TheBoardDetected, camParams, MarkerSize );

                if(probDetect > 0.0) {
                    boardDetect[i] = true;
                    RotationMatrics[i] = Mat::zeros(Rot3D.size(),CV_32FC1);
                    TransfMatrices[i] = Mat::zeros(Trans.size(),CV_32FC1);

                    RotationMatrics[i] = Rot3D.clone();
                    TransfMatrices[i] = Trans.clone();
                } else
                    boardDetect[i] = false;

                if(probDetect>0)
                {

                    // draw the world axes in the centre of the board
                    CvDrawingUtils::draw3dAxis(bgrImage,TheBoardDetected,camParams);


                    // convert 3x1 rotation vector to 3x3 rotation matrix
                    Rodrigues(TheBoardDetected.Rvec, Rot3D);

                }

                //cout << Rot3D << endl;

                Rot3D.release();
                Trans.release();
            }

            cout << RotationMatrics[0] << endl;



            sprintf(buffer, "%s/img_%03d.png", outputImageDirectory, n+1);

            //imwrite(buffer, bgrImage);

            // store only if all boards are detected
            if(boardDetect[0] && boardDetect[1] && boardDetect[2]) {
                imwrite(buffer, bgrImage);
                for(int i = 0; i < numBoards; i++) {
                    outputDataListFile << RotationMatrics[i] << endl << TransfMatrices[i] << endl;
                    //saveTransfMatrix(outputDataListFile, RotationMatrics[i], TransfMatrices[i]);
                }
            }


            //outputDataListFile <<

//			imshow("bgrOutput", bgrImage);
//            waitKey(1);

			//if(waitKey(30) >= 0) break;

            bgrImage.release();

		}
	} catch(exception &e) {
		cout << "Exception thrown: " << e.what() << endl;
	}

    return 0;
}
